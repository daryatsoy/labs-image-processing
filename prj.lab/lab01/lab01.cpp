#include <opencv2/opencv.hpp>

using namespace cv;

int main()
{
	int scale = 3;
	int w = scale * 256;
	int h = 60;
	Mat line(Mat::zeros(h, w, CV_8UC1));
	for (int i_row = 0; i_row < h; i_row++) {
		for (int i_col = 0; i_col < w; i_col++) {
			line.at<uint8_t>(i_row, i_col) = i_col / scale;
		}
	}

	Mat correct;
	line.convertTo(correct, CV_32FC1, 1 / 255.0, 0.0);
	float gamma = 2.2;
	pow(correct, gamma, correct);
	correct.convertTo(correct, CV_8UC1, 255);
	Mat couple[2] = { line.clone(), correct.clone() };
	Mat res1;
	vconcat(couple, 2, res1);
	imshow("task 1", res1);

	Mat stepline(Mat::zeros(h, w, CV_8UC1));
	for (int i_row = 0; i_row < h; i_row++) {
		for (int i_col = 0; i_col < w; i_col++) {
			stepline.at<uint8_t>(i_row, i_col) = int(i_col/30)*10+5;
		}
	}
	Mat stepcorrect;
	stepline.convertTo(stepcorrect, CV_32FC1, 1 / 255.0, 0.0);
	pow(stepcorrect, gamma, stepcorrect);
	stepcorrect.convertTo(stepcorrect, CV_8UC1, 255);
	Mat stepcouple[2] = { stepline.clone(), stepcorrect.clone() };
	Mat res2;
	vconcat(stepcouple, 2, res2);
	imshow("task 2", res2);
	Mat save_arr[2] = { res1, res2 };
	Mat save;
	vconcat(save_arr, 2, save);
	imwrite("../../../prj.lab/lab01/lab01.png", save);
	waitKey(0);
	return 0;
}