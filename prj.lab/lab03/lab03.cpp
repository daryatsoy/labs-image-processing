#include <opencv2/opencv.hpp>
#include <iostream>
//#include <opencv2/ximgproc.hpp>

using namespace cv;
using namespace std;

Mat GetGammaExpo()
{
	Mat result(1, 256, CV_8UC1);
	int y;
	for (int i = 0; i < 256; i++)
	{
		//y = int(exp(double(i)/50));
		y = int(sin(i * 0.1227) * 128 + 128);
		//y = -i+255;
		if (y > 255) { y = 255; }
		if (y < 0) { y = 0; }
		result.at<uint8_t>(i) = y;
	}
	return result;
}

Mat Negative()
{
	Mat result(1, 256, CV_8UC1);
	int y;
	for (int i = 0; i < 256; i++)
	{
		y = -i + 255;
		if (y > 255) { y = 255; }
		if (y < 0) { y = 0; }
		result.at<uint8_t>(i) = y;
	}
	return result;
}

Mat createHist(Mat src, int height, int scale)
{
	int hist[256]{ 0 }; // ���������� �������� i-�� �������
	for (int irow = 0; irow < src.rows; irow++) {
		for (int icol = 0; icol < src.cols; icol++) {
			hist[src.at<uint8_t>(irow, icol)]++;
		}
	}
	int n = sizeof(hist) / sizeof(hist[0]);
	int max_el;
	max_el = *max_element(hist, hist + n); // ������������ ���������� �������� ������ ��������
	Mat hist_img(Mat::zeros(height, 256 * scale, CV_8UC1));
	hist_img.setTo(255);
	for (int i = 0; i < 256; i++) {
		rectangle(hist_img, Point(i * scale, height - int(float(hist[i]) / max_el * height)), Point(i * scale + scale, height), Scalar::all(0), -1);
	}
	return hist_img;
}

int main()
{
	Mat image(imread("../../../testdata/cross_0256x0256.png", CV_8UC1));
	imwrite("../../../prj.lab/lab03/lab03.src.png", image);
	int height = 256; // ������ �����������
	int scale = 2; // ����������� ��������������� �� �����������
	Mat original_hist = createHist(image, height, scale);
	imwrite("../../../prj.lab/lab03/lab03.hist.src.png", original_hist);
	//imshow("original histogram", original_hist);

	// LUT
	// _________________________________________________
	Mat res_lut;
	Mat lut = GetGammaExpo();
	Mat graph_img(Mat::zeros(height, 256 * scale, CV_8UC1));
	graph_img.setTo(255);
	for (int i = 0; i < 256 - 1; i++) {
		line(graph_img, Point(i * scale, height - lut.at<uint8_t>(0, i)), Point(i * scale + scale, height - lut.at<uint8_t>(0, i + 1)), Scalar::all(0), 1);
	}
	imshow("LUT graph", graph_img);
	imwrite("../../../prj.lab/lab03/lab03.lut.png", graph_img);
	LUT(image, lut, res_lut);
	imshow("LUT", res_lut);
	imwrite("../../../prj.lab/lab03/lab03.lut.src.png", res_lut);
	Mat lut_arr[2] = { original_hist,  createHist(res_lut, height, scale) };
	Mat res_lut_mozaic;
	vconcat(lut_arr, 2, res_lut_mozaic);
	imshow("LUT histogram", res_lut_mozaic);
	imwrite("../../../prj.lab/lab03/lab03.hist.lut.src.png", res_lut_mozaic);
	// _________________________________________________

	// CLAHE
	// _________________________________________________

	Ptr<CLAHE> clahe = cv::createCLAHE();
	Mat res_clahe1;
	clahe->apply(image, res_clahe1);
	putText(res_clahe1, "40 8x8", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	clahe->setClipLimit(4);
	clahe->setTilesGridSize(Size(4, 4));
	Mat res_clahe2;
	clahe->apply(image, res_clahe2);
	putText(res_clahe2, "4 4x4", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	clahe->setClipLimit(40);
	clahe->setTilesGridSize(Size(32, 32));
	Mat res_clahe3;
	clahe->apply(image, res_clahe3);
	putText(res_clahe3, "40 32x32", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	imwrite("../../../prj.lab/lab03/lab03.clahe.1.png", res_clahe1);
	imwrite("../../../prj.lab/lab03/lab03.clahe.2.png", res_clahe2);
	imwrite("../../../prj.lab/lab03/lab03.clahe.3.png", res_clahe3);

	Mat clache_mozaic_arr[4] = { image, res_clahe1, res_clahe2, res_clahe3 };
	Mat hist_mozaic_arr0[2] = { original_hist, createHist(res_clahe1, height, scale) };
	Mat hist_mozaic_arr1[2] = { createHist(res_clahe2, height, scale), createHist(res_clahe3, height, scale) };
	imwrite("../../../prj.lab/lab03/lab03.hist.clahe.1.png", hist_mozaic_arr0[1]);
	imwrite("../../../prj.lab/lab03/lab03.hist.clahe.2.png", hist_mozaic_arr1[0]);
	imwrite("../../../prj.lab/lab03/lab03.hist.clahe.3.png", hist_mozaic_arr1[1]);
	Mat clache_mozaic, hist_mozaic, hist_mozaic0, hist_mozaic1;
	hconcat(clache_mozaic_arr, 4, clache_mozaic);
	vconcat(hist_mozaic_arr0, 2, hist_mozaic0);
	vconcat(hist_mozaic_arr1, 2, hist_mozaic1);
	Mat hist_mozaic_arr2[2] = { hist_mozaic0, hist_mozaic1 };
	hconcat(hist_mozaic_arr2, 2, hist_mozaic);
	imshow("clache", clache_mozaic);
	imshow("orig; 40 8x8;4 4x4; 40 32x32", hist_mozaic);

	// _________________________________________________

	Mat image_bin(imread("../../../testdata/bin.jpg", CV_8UC1));

	// global binarization
	// _________________________________________________

	Mat res_binar;
	threshold(image_bin, res_binar, 0, 255, THRESH_BINARY | THRESH_OTSU);
	//resize(res_binar, res_binar, Size(res_binar.cols * 400 / res_binar.rows, 400));
	//imshow("global otsu binary", res_binar);
	// _________________________________________________


	// niBlack
	// _________________________________________________

	int border_thickness = 16;
	double k = 1;
	double d = -25;
	Mat res_niBlack(Mat(image_bin.size(), CV_8UC1));
	Mat res_bordered;
	int thr_pixl;
	Scalar m, s;
	copyMakeBorder(image_bin, res_bordered, border_thickness, border_thickness, \
		border_thickness, border_thickness, BORDER_REPLICATE);
	Mat t;
	//resize(res_bordered, t, Size(res_bordered.cols * 400 / res_bordered.rows, 400));
	//imshow("REPLICATE", t);
	for (int i_row = 0; i_row < image_bin.rows; i_row++) {
		for (int i_col = 0; i_col < image_bin.cols; i_col++) {
			meanStdDev(res_bordered(Rect(i_col, i_row, 2 * border_thickness + 1, 2 * border_thickness + 1)), m, s);
			thr_pixl = m(0) + k * s(0) + d;
			if (image_bin.at<uint8_t>(i_row, i_col) > thr_pixl) {
				res_niBlack.at<uint8_t>(i_row, i_col) = 255;
			}
			else {
				res_niBlack.at<uint8_t>(i_row, i_col) = 0;
			}
		}
	}
	//resize(res_niBlack, res_niBlack, Size(res_niBlack.cols * 400 / res_niBlack.rows, 400));
	//imshow("niBlack binary", res_niBlack);

	// morphology
	// _________________________________________________
	int dilation_size = 1;
	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		Point(dilation_size, dilation_size));
	Mat res_dil_er;
	dilate(res_niBlack, res_dil_er, element);
	erode(res_dil_er, res_dil_er, element);
	//resize(res_dil_er, res_dil_er, Size(res_dil_er.cols * 400 / res_dil_er.rows, 400));
	//imshow("dil er", res_dil_er);
	// _________________________________________________


	// _________________________________________________
	//resize(res_niBlack, res_niBlack, Size(res_niBlack.cols * 400 / res_niBlack.rows, 400));
	//resize(image_bin, image_bin, Size(image_bin.cols * 400 / image_bin.rows, 400));
	putText(image_bin, "original", Point(image_bin.cols * 0.3, image_bin.rows * 0.1), FONT_HERSHEY_DUPLEX, 0.9, CV_RGB(120, 120, 120));
	putText(res_binar, "global binary", Point(image_bin.cols * 0.3, image_bin.rows * 0.1), FONT_HERSHEY_DUPLEX, 0.9, CV_RGB(120, 120, 120));
	putText(res_niBlack, "niblack", Point(image_bin.cols * 0.3, image_bin.rows * 0.1), FONT_HERSHEY_DUPLEX, 0.9, CV_RGB(120, 120, 120));
	putText(res_dil_er, "opening", Point(image_bin.cols * 0.3, image_bin.rows * 0.1), FONT_HERSHEY_DUPLEX, 0.9, CV_RGB(120, 120, 120));
	Mat binary_arr[4] = { image_bin, res_binar, res_niBlack, res_dil_er };
	Mat binary_mozaic;
	hconcat(binary_arr, 4, binary_mozaic);
	Mat res_bin_moz;
	resize(binary_mozaic, res_bin_moz, Size(binary_mozaic.cols * 400 / binary_mozaic.rows, 400));
	imshow("binary", res_bin_moz);

	Mat res6, res7, res8, res9;
	Mat res6_arr[2] = { image_bin, res_binar};
	hconcat(res6_arr, 2, res6);
	imwrite("../../../prj.lab/lab03/lab03.bin.global.png", res6);

	Mat res7_arr[2] = { image_bin, res_niBlack };
	hconcat(res7_arr, 2, res7);
	imwrite("../../../prj.lab/lab03/lab03.bin.local.png", res7);

	Mat res8_arr[2] = { image_bin, res_dil_er };
	hconcat(res8_arr, 2, res8);
	imwrite("../../../prj.lab/lab03/lab03.morph.png", res8);
	
	Mat visualmask1, visualmask2, mask;
	res_dil_er.convertTo(mask, CV_32FC1, 1 / 255.0, 0.0);
	mask.convertTo(mask, CV_8UC1, 1);
	bitwise_and(image_bin, res_dil_er, visualmask1, mask);
	Mat inverse = Negative();
	LUT(res_dil_er, inverse, res_dil_er);
	res_dil_er.convertTo(mask, CV_32FC1, 1 / 255.0, 0.0);
	mask.convertTo(mask, CV_8UC1, 1);
	bitwise_and(image_bin, res_dil_er, visualmask2, mask);
	Mat res9_arr[2] = { visualmask1, visualmask2 };
	hconcat(res9_arr, 2, res9);
	imwrite("../../../prj.lab/lab03/lab03.mask.png", res9);
	// _________________________________________________

	waitKey(0);
	return 0;
}