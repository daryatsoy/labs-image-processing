
#include <windows.h>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;

Mat brightness_moz(Mat image)
{
	Mat bgr[3];
	Mat empty(Mat::zeros(image.size(), CV_8UC1));
	split(image, bgr);
	Mat matrices2[4] = { empty.clone(), bgr[0].clone(), bgr[1].clone(), bgr[2].clone() };
	Mat channels;
	hconcat(matrices2, 4, channels);
	cvtColor(channels, channels, COLOR_GRAY2BGR);
	return channels;
}

Mat colors_moz(Mat image)
{
	Mat blue = image.clone() & cv::Scalar(255, 0, 0);
	Mat green = image.clone() & cv::Scalar(0, 255, 0);
	Mat red = image.clone() & cv::Scalar(0, 0, 255);

	Mat matrices[4] = { image, blue, green,red };
	Mat colors;
	hconcat(matrices, 4, colors);
	return colors;
}

Mat mozaika(Mat image)
{
	Mat channels = brightness_moz(image);
	Mat colors = colors_moz(image);
	Mat matrix[2] = { channels, colors };
	Mat matrres;
	vconcat(matrix, 2, matrres);
	return matrres;
}


int main()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	cout << "my directory is " << string(buffer) << "\n";
	Mat im(imread("../../../testdata/bird.png"));
	int height = 800;

	vector<int> compression_params;
	compression_params.push_back(IMWRITE_JPEG_QUALITY);
	compression_params.push_back(95);

	imwrite("../../../result/95.jpg", im, compression_params);
	compression_params.pop_back();
	compression_params.push_back(65);
	imwrite("../../../result/65.jpg", im, compression_params);

	Mat im95(imread("../../../result/95.jpg"));
	Mat im65(imread("../../../result/65.jpg"));

	Mat bright_orig = brightness_moz(im);
	Mat colors_orig = colors_moz(im);
	Mat bright_95 = brightness_moz(im95);
	Mat colors_95 = colors_moz(im95);
	Mat bright_65 = brightness_moz(im65);
	Mat colors_65 = colors_moz(im65);
	Mat matrix_b[3] = { bright_orig, bright_95, bright_65 };
	Mat matrres_b;
	vconcat(matrix_b, 3, matrres_b);
	Mat showb, showc;
	resize(matrres_b, showb, Size(int(matrres_b.cols * height / matrres_b.rows), height), 0, 0, INTER_LINEAR);
	imshow("brightness", showb);
	Mat matrix_c[3] = { colors_orig, colors_95, colors_65 };
	Mat matrres_c;
	vconcat(matrix_c, 3, matrres_c);
	resize(matrres_c, showc, Size(int(matrres_c.cols * height / matrres_c.rows), height), 0, 0, INTER_LINEAR);
	imshow("chanels", showc);
	Mat alls[2] = { matrres_c, matrres_b };
	Mat lab2;
	vconcat(alls, 2, lab2);
	imwrite("../../../prj.lab/lab02/lab02.2.png", lab2);
	Mat bgr_orig[3];
	Mat bgr_95[3];
	Mat bgr_65[3];
	split(im, bgr_orig);
	split(im95, bgr_95);
	split(im65, bgr_65);
	Mat diff_cb1 = abs(bgr_orig[0] - bgr_95[0]) * 20;
	Mat diff_cb2 = abs(bgr_orig[0] - bgr_65[0]) * 20;
	Mat diff_cg1 = abs(bgr_orig[1] - bgr_95[1]) * 20;
	Mat diff_cg2 = abs(bgr_orig[1] - bgr_65[1]) * 20;
	Mat diff_cr1 = abs(bgr_orig[2] - bgr_95[2]) * 20;
	Mat diff_cr2 = abs(bgr_orig[2] - bgr_65[2]) * 20;
	Mat hsv_orig[3];
	Mat hsv_95[3];
	Mat hsv_65[3];
	cvtColor(im, im, COLOR_BGR2HSV);
	cvtColor(im95, im95, COLOR_BGR2HSV);
	cvtColor(im65, im65, COLOR_BGR2HSV);
	split(im, hsv_orig);
	split(im95, hsv_95);
	split(im65, hsv_65);
	Mat diff_b1 = abs(hsv_orig[1] - hsv_95[1]) * 10;
	Mat diff_b2 = abs(hsv_orig[1] - hsv_65[1]) * 10;
	Mat arr95[4] = { diff_cb1 , diff_cg1, diff_cr1, diff_b1 };
	Mat arr65[4] = { diff_cb2 , diff_cg2, diff_cr2, diff_b2 };
	Mat res1, res2, res;
	hconcat(arr95, 4, res1);
	hconcat(arr65, 4, res2);
	Mat arr[2] = { res1, res2 };
	vconcat(arr, 2, res);
	Mat show;
	//resize(res, show, Size(int(res.cols * height / res.rows), height));
	//imshow("res", show);
	imwrite("../../../prj.lab/lab02/lab02.png", res);


	waitKey(0);
	return 0;
}