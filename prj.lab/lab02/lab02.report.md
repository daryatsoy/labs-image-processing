﻿## Работа 2. Визуализация искажений jpeg-сжатия
автор: Цой Д.А.
дата: @time_stemp@

<!-- url: https://gitlab.com/daryatsoy/labs-image-processing/-/tree/master/prj.lab/lab02 -->

### Задание
Для исходного изображения (сохраненного без потерь) создать jpeg версии с двумя уровнями качества (например, 95 и 65). Вычислить и визуализировать на одной “мозаике” исходное изображение, результаты сжатия, поканальные и яркостные различия.

### Результаты

![](../testdata/cross_0256x0256.png)
Рис 1. Исходное изображение

![](lab02.png)
Рис 2. Результаты работы программы 

### Текст программы

```cpp

#include <windows.h>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;

Mat brightness_moz(Mat image)
{
	Mat bgr[3];
	Mat empty(Mat::zeros(image.size(), CV_8UC1));
	split(image, bgr);
	Mat matrices2[4] = { empty.clone(), bgr[0].clone(), bgr[1].clone(), bgr[2].clone() };
	Mat channels;
	hconcat(matrices2, 4, channels);
	cvtColor(channels, channels, COLOR_GRAY2BGR);
	return channels;
}

Mat colors_moz(Mat image)
{
	Mat blue = image.clone() & cv::Scalar(255, 0, 0);
	Mat green = image.clone() & cv::Scalar(0, 255, 0);
	Mat red = image.clone() & cv::Scalar(0, 0, 255);

	Mat matrices[4] = { image, blue, green,red };
	Mat colors;
	hconcat(matrices, 4, colors);
	return colors;
}

Mat mozaika(Mat image)
{
	Mat channels = brightness_moz(image);
	Mat colors = colors_moz(image);
	Mat matrix[2] = { channels, colors };
	Mat matrres;
	vconcat(matrix, 2, matrres);
	return matrres;
}


int main()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	cout << "my directory is " << string(buffer) << "\n";
	Mat im(imread("../../../testdata/bird.png"));
	int height = 800;

	vector<int> compression_params;
	compression_params.push_back(IMWRITE_JPEG_QUALITY);
	compression_params.push_back(95);

	imwrite("../../../result/95.jpg", im, compression_params);
	compression_params.pop_back();
	compression_params.push_back(65);
	imwrite("../../../result/65.jpg", im, compression_params);

	Mat im95(imread("../../../result/95.jpg"));
	Mat im65(imread("../../../result/65.jpg"));


	Mat bright_orig = brightness_moz(im);
	Mat colors_orig = colors_moz(im);
	Mat bright_95 = brightness_moz(im95);
	Mat colors_95 = colors_moz(im95);
	Mat bright_65 = brightness_moz(im65);
	Mat colors_65 = colors_moz(im65);
	Mat matrix_b[3] = { bright_orig, bright_95, bright_65 };
	Mat matrres_b;
	vconcat(matrix_b, 3, matrres_b);
	Mat showb, showc;
	resize(matrres_b, showb, Size(int(matrres_b.cols * height / matrres_b.rows), height), 0, 0, INTER_LINEAR);
	imshow("brightness", showb);
	Mat matrix_c[3] = { colors_orig, colors_95, colors_65 };
	Mat matrres_c;
	vconcat(matrix_c, 3, matrres_c);
	resize(matrres_c, showc, Size(int(matrres_c.cols * height / matrres_c.rows), height), 0, 0, INTER_LINEAR);
	imshow("chanels", showc);
	Mat alls[2] = { matrres_c, matrres_b };
	Mat lab2;
	vconcat(alls, 2, lab2);
	imwrite("../../../prj.lab/lab02/lab02.png", lab2);


	waitKey(0);
	return 0;
}
```