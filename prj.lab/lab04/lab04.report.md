## Работа 4. Использование 
автор: Полевой Д.В.

<!-- url: https://gitlab.com/daryatsoy/labs-image-processing/-/tree/master/prj.lab/lab04 -->

### Задание
1. Сгенерировать серое тестовое изображение из квадратов и кругов с разными уровнями яркости (0, 127 и 255) так, чтобы присутствовали все сочетания.
2. Применить первый линейный фильтр и сделать визуализацию результата $F_1$.
3. Применить второй линейный фильтр и сделать визуализацию результата $F_2$.
4. Вычислить $R=\sqrt{F_1^2 + F_2^2}$  и сделать визуализацию R.

### Результаты

![](lab04.src.png)
Рис. 1. Исходное тестовое изображение

![](lab04.viz_dx.png)
Рис. 2. Визуализация результата $F_1$ применения фильтра

![](lab04.viz_dy.png)
Рис. 3. Визуализация результата $F_2$ применения фильтра

![](lab04.viz_gradmod.png)
Рис. 4. Визуализация модуля градиента $R$

### Текст программы

```cpp
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main()
{
	int size = 150;
	Mat empty(Mat::zeros(Size(size, size), CV_8UC1));
	Mat result[3] = { empty.clone().setTo(255),empty.clone().setTo(0), empty.clone().setTo(127) };
	Mat result2[3] = { empty.clone().setTo(127), empty.clone().setTo(255), empty.clone().setTo(0) };
	circle(result[0], Point(int(size / 2), int(size / 2)), int(size / 3), Scalar::all(127), -1);
	circle(result[1], Point(int(size / 2), int(size / 2)), int(size / 3), Scalar::all(255), -1);
	circle(result[2], Point(int(size / 2), int(size / 2)), int(size / 3), Scalar::all(0), -1);
	circle(result2[0], Point(int(size / 2), int(size / 2)), int(size / 3), Scalar::all(255), -1);
	circle(result2[1], Point(int(size / 2), int(size / 2)), int(size / 3), Scalar::all(0), -1);
	circle(result2[2], Point(int(size / 2), int(size / 2)), int(size / 3), Scalar::all(127), -1);
	Mat r1, r2, im;
	hconcat(result, 3, r1);
	hconcat(result2, 3, r2);
	Mat r[2] = { r1, r2 };
	vconcat(r, 2, im);
	imshow("original", im);
	imwrite("../../../prj.lab/lab04/lab04.src.png", im);

	Mat f1, f2, f3;
	Mat kernel1(Mat::zeros(2, 2, CV_8SC1));
	Mat kernel2 = kernel1.clone();
	kernel1.at<int8_t>(0, 0) = 1.;
	kernel1.at<int8_t>(1, 1) = -1.;
	kernel2.at<int8_t>(0, 1) = 1.;
	kernel2.at<int8_t>(1, 0) = -1.;
	filter2D(im, f1, CV_32F, kernel1, Point(-1, -1), 0, BORDER_REFLECT);
	filter2D(im, f2, CV_32F, kernel2, Point(-1, -1), 0, BORDER_REFLECT);

	f1 = ((f1 + 255) / 2);
	f2 = ((f2 + 255) / 2);
	pow(f1.mul(f1) + f2.mul(f2), 0.5, f3);
	f1.convertTo(f1, CV_8UC1);
	f2.convertTo(f2, CV_8UC1);
	f3.convertTo(f3, CV_8UC1);
	imshow("operator 1", f1);
	imshow("operator 2", f2);
	imshow("mean", f3);
	imwrite("../../../prj.lab/lab04/lab04.viz_dx.png", f1);
	imwrite("../../../prj.lab/lab04/lab04.viz_dy.png", f2);
	imwrite("../../../prj.lab/lab04/lab04.viz_gradmod.png", f3);

	waitKey(0);
	return 0;
}
```